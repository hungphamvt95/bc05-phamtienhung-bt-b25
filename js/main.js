const BASE_URL = "https://6361d02b7521369cd05eba1d.mockapi.io";
var editBtnToggle = false;
var idEdit = null;
var isCompleteEdit = false;
function fetchAllToDo() {
  editBtnToggle = false;
  axios({
    url: `${BASE_URL}/todosapp`,
    method: "GET",
  })
    .then(function (res) {
      renderToDoList(res.data);
      console.log("res: ", res);
      turnOffLoading();
    })
    .catch(function (err) {
      console.log("err: ", err);
      turnOffLoading();
    });
}
fetchAllToDo();

function isCompleted(idComplete) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todosapp/${idComplete}`,
    method: "GET",
  })
    .then((res) => {
      var data = {
        id: res.data.id,
        taskDesc: res.data.taskDesc,
        isComplete: !res.data.isComplete,
      };
      axios({
        url: `${BASE_URL}/todosapp/${data.id}`,
        method: "PUT",
        data: data,
      })
        .then((res) => {
          fetchAllToDo();
          console.log("res: ", res);
        })
        .catch((err) => {
          turnOffLoading();
          console.log("err: ", err);
        });
      console.log("res: ", res);
      console.log("data: ", data);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
}

function deleteToDo(idToDo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todosapp/${idToDo}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchAllToDo();
      console.log("res: ", res);
    })
    .catch((err) => {
      turnOffLoading();
      console.log("err: ", err);
    });
}

function addToDo() {
  turnOnLoading();
  var data = getFormData();
  var newEditToDo = {
    taskDesc: data.taskDesc,
    isComplete: isCompleteEdit,
  };
  var newToDo = {
    taskDesc: data.taskDesc,
    isComplete: false,
  };
  if (!editBtnToggle) {
    axios({
      url: `${BASE_URL}/todosapp`,
      method: "POST",
      data: newToDo,
    })
      .then((res) => {
        fetchAllToDo();
        console.log("res: ", res);
        resetInputText();
      })
      .catch((err) => {
        turnOffLoading();
        console.log("err: ", err);
      });
  } else {
    axios({
      url: `${BASE_URL}/todosapp/${idEdit}`,
      method: "PUT",
      data: newEditToDo,
    })
      .then((res) => {
        fetchAllToDo();
        console.log("res: ", res);
        resetInputText();
      })
      .catch((err) => {
        turnOffLoading();
        console.log("err: ", err);
      });
  }
}

function editToDo(idToDo) {
  axios({
    url: `${BASE_URL}/todosapp/${idToDo}`,
    method: "GET",
  })
    .then((res) => {
      console.log("res: ", res);
      showDataToForm(res.data.taskDesc);
      idEdit = res.data.id;
      isCompleteEdit = res.data.isComplete;
      editBtnToggle = true;
    })
    .catch((err) => {
      console.log("err: ", err);
    });
}
