function renderToDoList(todos) {
  var contentHTML = "";
  todos.forEach(function (item) {
    var contentTr = `
    <div class="todo__item">
    <p>${item.isComplete ? `<s>${item.taskDesc}</s>` : `${item.taskDesc}`}</p>
    <div class="todo__btn">
    <button onclick="isCompleted(${item.id})" class="btnStyle">Complete</button>
    <button onclick="editToDo(${item.id})" class="btnStyle">Sửa</button>
    <button onclick="deleteToDo(${item.id})" class="btnStyle">Xóa</button>
    </div>
    </div>`;
    contentHTML += contentTr;
  });

  document.querySelector(".todo__list").innerHTML = contentHTML;
}

function getFormData() {
  var taskDesc = document.querySelector(".input__text").value;
  return {
    taskDesc: taskDesc,
  };
}

function showDataToForm(data) {
  document.querySelector(".input__text").value = data;
}

function resetInputText() {
  document.querySelector(".input__text").value = "";
}

function turnOnLoading() {
  document.querySelector(".loading_screen").style.display = "flex";
}

function turnOffLoading() {
  document.querySelector(".loading_screen").style.display = "none";
}
